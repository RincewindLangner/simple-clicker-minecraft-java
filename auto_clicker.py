#!/usr/bin/python3.8
# https://pypi.org/project/pynput/
from pynput.keyboard import Key, Listener
from pynput.mouse import Button, Controller

mouse = Controller()

def on_press(key):
	print('{0} pressed'.format(
		key))
	try:
		if key.char == 'y':
			print('great')  # if the character is a 'y'
			mouse.press(Button.right)
			# mouse.press(Button.left)
		else:
			print('not great')  # if character is not 'y'
	except:
		print('very wrong')  # if other keys are pressed like the ctrl key


def on_release(key):
	print('{0} release'.format(key))
	if key == Key.esc:
		mouse.release(Button.right)
		# mouse.release(Button.left)
		# Stop listener
		return False


# Collect events until released
with Listener(
		on_press=on_press,
		on_release=on_release) as listener:
	listener.join()
