# Important
## Devolepment has stopped on this project. It has continued in afk-minecraft 
https://gitlab.com/RincewindLangner/afk-minecraft 

# Auto clicker for Minecraft Java

Made in Python3.8 for a Linux based system,this program will currently hold down the right mouse button for you to allow for automated farms such as fishing. 

To enable this run the program in a terminal and when you are ready press the 'y' key. To cancel hit the 'Esc' key. This will close the auto clicker. 

If you want to keep it open but stop the clicker press the right mouse button. From there you can press the 'y' key again to start auto clicking again. 

## Requierments 

- Python3.8
- pynput 1.6.5

Designed to work in a linux based system but could work on others if Python3.8 is installed. 